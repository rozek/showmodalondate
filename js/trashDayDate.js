!function bigTrashDay () {

const modal = document.querySelector(".modal");
const trigger = document.querySelector(".trigger");
const closeButton = document.querySelector(".close-button");
const modalContent = document.getElementById('modal-content');

function toggleModal() {
    modal.classList.toggle("show-modal");
}

function windowOnClick(event) {
    if (event.target === modal) {
        toggleModal();
    }
}

trigger.addEventListener("click", toggleModal);
closeButton.addEventListener("click", toggleModal);
window.addEventListener("click", windowOnClick);

const t = moment().format('l'); 
const y = moment().subtract(1, 'days').format('l')

const tDay = ["2019-06-14", "2019-06-28", "2019-07-12", "2019-07-26", "2019-08-09", "2019-08-23", "2019-09-06", "2019-09-20", "2019-10-04", "2019-10-18", "2019-11-16", "2019-11-29", "2019-12-13", "2019-12-29" ]

const tDay2 = ["2019-06-26", "2019-07-10", "2019-07-24", "2019-08-07", "2019-08-21", "2019-09-04", "2019-09-18", "2019-10-16", "2019-10-30", "2019-11-14", "2019-11-27", "2019-12-11", "2019-12-27" ]

//format daty do danych z tablicy
const tDayFormater = el => moment(el).format('l')
const handletDay = tDay.map(tDayFormater)
const handletDay2 = tDay2.map(tDayFormater)

const isToday = el => el === t
const isDate = handletDay.filter(isToday); 
const isDate2 = handletDay2.filter(isToday);

const isYesterday = el => el === y
const isDateYester = handletDay.filter(isYesterday); 
const isDate2Yester = handletDay2.filter(isYesterday);

if ((isDate === undefined || isDate.length !== 0) || (isDateYester === undefined || isDateYester.length !== 0)) {
    toggleModal();  
    modalContent.textContent += 'Wywóz śmieci w regionie Wiosny Ludów 36 -46 ';
    modalContent.textContent += 'Wywóz śmieci w regionie Boh. Warszawy 83abc ';
}
if ((isDate2 === undefined || isDate2.length !== 0) || (isDate2Yester === undefined || isDate2Yester.length !== 0)) {
    toggleModal();  
    modalContent.textContent += 'Wywóz śmieci w regionie Duńska 4-10 ';
    modalContent.textContent += 'Wywóz śmieci w regionie Duńska 38-44a ';
    modalContent.textContent += 'Wywóz śmieci w regionie Duńska 46-52 ';
}

}();